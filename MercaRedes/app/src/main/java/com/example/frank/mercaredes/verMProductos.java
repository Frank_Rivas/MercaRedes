package com.example.frank.mercaredes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.squareup.picasso.Picasso;
import org.json.JSONException;
import org.json.JSONObject;

public class verMProductos extends AppCompatActivity{
    private int productId;
    private Button modificar;
    private Button eliminar;
    private EditText descripcion;
    private EditText precio;
    private EditText stock;
    private EditText nombre;
    private ImageView imgProd;
    private String urlProd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_mproductos);
        modificar = (Button)findViewById(R.id.button12);
        eliminar = (Button)findViewById(R.id.button11);
        nombre = (EditText)findViewById(R.id.editText12);
        stock = (EditText)findViewById(R.id.editText16);
        precio = (EditText)findViewById(R.id.editText15);
        descripcion = (EditText)findViewById(R.id.editText13);
        imgProd = (ImageView) findViewById(R.id.imageView5);



        stock.setEnabled(false);

        productId = getIntent().getIntExtra("idProd", 0);
        urlProd = "https://pseesapiproducts.herokuapp.com/products/"+productId;
        Ion.with(verMProductos.this)
                .load("GET", urlProd)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if (e == null) {
                            try {
                                JSONObject response = new JSONObject(result);
                                String name = response.optString("product_name");
                                String desc = response.optString("description");
                                String stockProd = response.optString("stock");
                                String price = response.optString("price");
                                String urlImage = "http:" + response.optString("image");

                                nombre.setText(name);
                                descripcion.setText(desc);
                                precio.setText(price);
                                stock.setText(stockProd);
                                Picasso.with(verMProductos.this).load(urlImage).into(imgProd);

                                Toast.makeText(verMProductos.this, name, Toast.LENGTH_SHORT).show();
                            } catch (JSONException eJson){
                                eJson.printStackTrace();
                            }


                        }
                    }
                });

        modificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(verMProductos.this, "About to push changes to: "+productId, Toast.LENGTH_SHORT).show();
                Ion.with(verMProductos.this)
                        .load("PUT", urlProd)
                        .setBodyParameter("description", descripcion.getText().toString())
                        .setBodyParameter("price", precio.getText().toString())
                        .setBodyParameter("product_name", nombre.getText().toString())
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception ePut, String result) {

                                if(ePut == null){
                                    Log.e("App", result);
                                    Toast.makeText(verMProductos.this, "Put successful", Toast.LENGTH_SHORT);
                                    finish();
                                } else {
                                    ePut.printStackTrace();
                                    Toast.makeText(verMProductos.this, "Error updating", Toast.LENGTH_SHORT);
                                }
                            }
                        });


            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(verMProductos.this, "About to delete"+productId, Toast.LENGTH_SHORT).show();
                Ion.with(verMProductos.this)
                        .load("DELETE", urlProd)
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception eDel, String result) {
                                if(eDel==null){
                                    Toast.makeText(verMProductos.this, "Delete successful", Toast.LENGTH_SHORT);
                                    finish();
                                } else {
                                    Toast.makeText(verMProductos.this, "Error deleting", Toast.LENGTH_SHORT);
                                }
                            }
                        });
            }
        });



    }
}
