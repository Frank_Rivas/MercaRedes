package com.example.frank.mercaredes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static com.example.frank.mercaredes.R.id.imageView6;

public class MainActivity extends AppCompatActivity{

    private ListView lista = null;
    private ArrayList<Product> arrayProd = null;
    private AdaptadorProducto adapter = null;
    private ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = (ListView)findViewById(R.id.listView);
        arrayProd = new ArrayList<>();
        new listarProductos().execute();
        imagen = (ImageView) findViewById(imageView6);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(MainActivity.this, ProductActivity.class);
                int idProd = arrayProd.get(position).getId();
                i.putExtra("ids", idProd);
                startActivity(i);
            }
        });

    }


    public class listarProductos extends AsyncTask<Void, Void, String> {

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0);
        Integer userId = pref.getInt("id", 0);

        String str="https://pseesapiproducts.herokuapp.com/catalog/" + userId;
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";

        @Override
        protected void onPostExecute(String result) {

            JSONObject json = null;
            try {

                JSONArray jsonarray = new JSONArray(result);
                for(int i=0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    String id = jsonobject.getString("id");
                    int ids = Integer.parseInt(id);
                    Log.e(id, "Valor enviado");
                    String nombre = jsonobject.getString("product_name");
                    String cont = jsonobject.getString("description");
                    double prec = Double.parseDouble(jsonobject.getString("price"));
                    String base = jsonobject.getString("image");
                    Log.e(base,"Miremos la img");
                    String conca = "http:" + base;
                    Log.e(conca,"Concatenada");

                    arrayProd.add(new Product(ids, conca,nombre,cont,prec));
                    addList();

                    Log.e("App", nombre);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try
            {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background", ex);
            }
            return result;
        }
    }

    private void addList(){

        adapter = new AdaptadorProducto(this, arrayProd);
        lista.setAdapter(adapter);
    }
}
