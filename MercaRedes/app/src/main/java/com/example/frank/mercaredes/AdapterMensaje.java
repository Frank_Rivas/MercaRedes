package com.example.frank.mercaredes;

/**
 * Created by Cristian on 17/06/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Cristian on 12/06/2017.
 */

public class AdapterMensaje  extends BaseAdapter {

    private ArrayList<mensajes>arrayList;
    private Context context;
    private LayoutInflater layoutInflater;


    public AdapterMensaje( Context context,ArrayList arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public int getCount( ) {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View visibles= layoutInflater.inflate(R.layout.enviados, parent ,false);
        TextView fecha=(TextView)visibles.findViewById(R.id.Fecha);
        TextView texto=(TextView)visibles.findViewById(R.id.Texto_mensaje);

        fecha.setText(arrayList.get(position).getFecha());
        texto.setText(arrayList.get(position).getTexto());



        return visibles;


    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public ArrayList<mensajes> getCurrentCheckedPosition() {
        return this.arrayList;
    }





}