package com.example.frank.mercaredes;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class Main3Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    int id_client;
    int id_supplier;
    int id_order,fragment;
    Productos_Mensaje  parametro;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        parametro=(Productos_Mensaje) getIntent().getExtras().getSerializable("parametro");

        id_client=parametro.getId_client();
        id_supplier=parametro.getId_supplier();
        id_order=parametro.getId_order();
        fragment=parametro.getFragment();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.icono);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               //Toast.makeText(getApplicationContext(),"presionado", Toast.LENGTH_SHORT).show();
               Intent i= new Intent(getBaseContext(),Enviar.class);
                 i.putExtra("parametro", parametro);
                startActivity(i);

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        FragmentManager fragmentManager=getSupportFragmentManager();
        Bundle bundle=new Bundle();
        bundle.putInt("parametro", id_client);
        bundle.putInt("parametro2",id_supplier);
        bundle.putInt("parametro3",fragment);
        bundle.putInt("parametro4",id_order);
        Fragment_recibidos nuevo=new Fragment_recibidos();
        nuevo.setArguments(bundle);

        fragmentManager.beginTransaction().replace(R.id.navegador_mensaje,nuevo).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager=getSupportFragmentManager();

        if (id == R.id.nav_camera) {
            Bundle bundle=new Bundle();
            bundle.putInt("parametro", id_client);
            bundle.putInt("parametro2",id_supplier);
            bundle.putInt("parametro3",fragment);
            bundle.putInt("parametro4",id_order);
            Fragment_recibidos nuevo=new Fragment_recibidos();
            nuevo.setArguments(bundle);

            fragmentManager.beginTransaction().replace(R.id.navegador_mensaje,nuevo).commit();

        } else if (id == R.id.nav_gallery) {
            Bundle bundle=new Bundle();
            bundle.putInt("parametro", id_client);
            bundle.putInt("parametro2",id_supplier);
            bundle.putInt("parametro3",fragment);
            bundle.putInt("parametro4",id_order);
            Fragment_enviados nuevo=new Fragment_enviados();
            nuevo.setArguments(bundle);

            fragmentManager.beginTransaction().replace(R.id.navegador_mensaje,nuevo).commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
