package com.example.frank.mercaredes;

/**
 * Created by usuario on 12/06/2017.
 */

public class Product {

    private int id;
    private String image;
    private String titulo;
    private String contenido;
    private Double precio;

    public Product(int id,String image, String titulo, String contenido, double precio) {
        this.image = image;
        this.titulo = titulo;
        this.contenido = contenido;
        this.precio = precio;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getImage() {
       return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
}
