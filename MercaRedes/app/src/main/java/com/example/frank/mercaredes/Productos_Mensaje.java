package com.example.frank.mercaredes;

/**
 * Created by Cristian on 17/06/2017.
 */

import java.io.Serializable;

/**
 * Created by Cristian on 16/06/2017.
 */
public class Productos_Mensaje implements  Comparable<Productos_Mensaje>, Serializable {

    private String date;
    private int id_supplier;
    private int id_client;
    private  int id_order;
    private String status;
    private int Fragment;

    public Productos_Mensaje(int id_order, int id_supplier, int id_client,String date,String status, int Fragment) {
        this.date = date;
        this.id_supplier = id_supplier;
        this.id_client = id_client;
        this.id_order = id_order;
        this.status = status;
        this.Fragment=Fragment;
    }

    public int getFragment() {
        return Fragment;
    }

    public void setFragment(int fragment) {
        Fragment = fragment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId_supplier() {
        return id_supplier;
    }

    public void setId_supplier(int id_supplier) {
        this.id_supplier = id_supplier;
    }

    public int getId_client() {
        return id_client;
    }

    public void setId_client(int id_client) {
        this.id_client = id_client;
    }

    public int getId_order() {
        return id_order;
    }

    public void setId_order(int id_order) {
        this.id_order = id_order;
    }

    @Override
    public int compareTo( Productos_Mensaje o) {
        if (this.id_order < o.id_order) {
            return -1;
        }
        else
        if (this.id_order > o.id_order) {
            return 1;
        }
        else
            return 0;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}