package com.example.frank.mercaredes;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class Carrito extends AppCompatActivity {

    private ListView listaCarrito = null;
    private ArrayList<ItemCarrito> arrayItem = null;
    private AdaptadorCarrito adapter = null;
    private Button pedido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrito);

        final MyApplication myApp = MyApplication.getInstance();


        pedido = (Button) findViewById(R.id.hacerPedido);
        listaCarrito = (ListView) findViewById(R.id.listCarrito);



        pedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Hacer el pedido*/
                final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0); // 0 - for private mode
                ArrayList<ItemCarrito> tempList = myApp.getItemsCarrito();
                if(!tempList.isEmpty()){
                    int[] prodIds = new int[tempList.size()];
                    int[] prodQuan = new int[tempList.size()];
                    int clientId = pref.getInt("id", 0);

                    for (int i=0; i < tempList.size(); i++){
                        prodIds[i] = tempList.get(i).getId();
                        prodQuan[i] = tempList.get(i).getCantidad();
                    }

                    Date ent = new Date(new Date().getTime() + 3*86400*1000);
                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String entrega = formatter.format(ent);

                    Log.e("Productos", Arrays.toString(prodIds));

                    Ion.with(Carrito.this)
                            .load("POST", "https://pseesapipedidos.herokuapp.com/orders")
                            .setBodyParameter("client_id", String.valueOf(clientId))
                            .setBodyParameter("product_list", Arrays.toString(prodIds))
                            .setBodyParameter("quantity_list", Arrays.toString(prodQuan))
                            .setBodyParameter("date_delivery", entrega)
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                    if(!result.isEmpty()){
                                        Log.e("Result: ", result);
                                    /* Limpiar la lista */
                                        myApp.setItemsCarrito(new ArrayList<ItemCarrito>());
                                        finish();
                                    } else {
                                        Log.e("Result: ", "Failure");
                                    }
                                }
                            });
                } else {
                    Toast.makeText(Carrito.this, "Tu carrito esta vacio", Toast.LENGTH_SHORT).show();
                }




            }
        });

        adapter = new AdaptadorCarrito(this, myApp.getItemsCarrito());
        listaCarrito.setAdapter(adapter);
    }


}
