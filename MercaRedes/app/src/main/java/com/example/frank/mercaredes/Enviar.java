package com.example.frank.mercaredes;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class Enviar extends AppCompatActivity {

    EditText editText;
    Button button;
    int id_order;
    int fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment);

        Productos_Mensaje parametro=(Productos_Mensaje)getIntent().getExtras().getSerializable("parametro");

        fragment=parametro.getFragment();
        id_order=parametro.getId_order();

        editText = (EditText) findViewById(R.id.texto_mensaje);
        button = (Button) findViewById(R.id.boton_mensaje);

        button.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          if (editText.length()>10)
                                          {
                                              new httpPost().execute();
                                              editText.setText("");}

                                          else
                                          {
                                              Toast.makeText(getApplicationContext(),
                                                      "Introdusca un mensaje con mas de 10 caracteres",
                                                      Toast.LENGTH_SHORT).show();
                                          }
                                      }
                                  }
        );



    }



    public class httpPost extends AsyncTask<String, Void, String> {

        String mensaje = editText.getText().toString();
        Boolean route=null;

        protected void onPreExecute(){}

        @Override
        protected String doInBackground(String... params) {
            if (fragment==1)
                route=false;
            else
                route=true;

            try {



                URL url = new URL("https://apimessage.herokuapp.com/order_messages");

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("order_id", id_order);
                postDataParams.put("message", mensaje);
                postDataParams.put("route",route);
                Log.e("params", postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode = conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK || responseCode == HttpsURLConnection.HTTP_CREATED) {

                    BufferedReader in = new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line = "";

                    while ((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                } else {
                    return new String("false : " + responseCode);
                }
            } catch (Exception e) {
                return new String("Exception: " + e.getMessage());
            }

        }
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
        }

    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }


}
