package com.example.frank.mercaredes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by usuario on 18/06/2017.
 */

public class AdaptadorCarrito extends BaseAdapter {
    private ArrayList<ItemCarrito> arrayListProd;
    private Context context;
    private LayoutInflater layoutInflater;

    public AdaptadorCarrito(Context context, ArrayList<ItemCarrito> arrayListProd) {
        this.context = context;
        this.arrayListProd = arrayListProd;
    }

    @Override
    public int getCount() {
        {
            return arrayListProd.size();
        }
    }

    @Override
    public Object getItem(int position) {
        {
            return arrayListProd.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        {
            return position;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vista = layoutInflater.inflate(R.layout.layout_carrito, parent ,false);
        ImageView img = (ImageView) vista.findViewById(R.id.imageCarrito);
        TextView nombre = (TextView) vista.findViewById(R.id.nombreProd);
        TextView desc = (TextView) vista.findViewById(R.id.descProd);
        TextView prec = (TextView) vista.findViewById(R.id.precioProd);
        TextView cant = (TextView) vista.findViewById(R.id.cantidadProd);

        Picasso.with(context).load(arrayListProd.get(position).getImage()).into(img);
        nombre.setText(arrayListProd.get(position).getTitulo());
        desc.setText(arrayListProd.get(position).getContenido());
        prec.setText(String.valueOf(arrayListProd.get(position).getPrecio()));
        cant.setText(String.valueOf(arrayListProd.get(position).getCantidad()));

        Button descar = (Button) vista.findViewById(R.id.Descartar);
        descar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayListProd.remove(position);
                notifyDataSetChanged();
            }
        });

        return vista;
    }

}
