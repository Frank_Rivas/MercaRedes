package com.example.frank.mercaredes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import Entidades.Valuation;

/**
 * Created by usuario on 16/06/2017.
 */

public class ValuationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private LayoutInflater inflater;
    List<Valuation> dataValuation = Collections.emptyList();
    Valuation current;
    int currentPOs=0;

    public ValuationAdapter(Context context, List<Valuation> dataValuation){
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.dataValuation=dataValuation;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int ViewType){
        View view = inflater.inflate(R.layout.container_valuation, parent,false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position){

        MyHolder myHolder =(MyHolder) holder;
        Valuation current = dataValuation.get(position);
        myHolder.tvNombre.setText(current.firstName+" "+current.lastName);
        myHolder.tvCommentary.setText(current.commentary);
        myHolder.tvScore.setText(Float.toString(current.score));

        myHolder.ratingBarValuation.setIsIndicator(true);
        myHolder.ratingBarValuation.setNumStars(5);
        myHolder.ratingBarValuation.setMax(10);
        myHolder.ratingBarValuation.setStepSize(0.5f);
        myHolder.ratingBarValuation.setRating((current.score/10)*5);


    }

    @Override
    public int getItemCount(){
        return dataValuation.size();
    }

    class MyHolder extends RecyclerView.ViewHolder{

        TextView tvNombre;
        TextView tvCommentary;
        TextView tvScore;
        RatingBar ratingBarValuation;

        public MyHolder(View itemView){
            super(itemView);
            tvNombre=(TextView) itemView.findViewById(R.id.tvNombre);
            tvCommentary = (TextView) itemView.findViewById(R.id.tvCommentary);
            tvScore = (TextView) itemView.findViewById(R.id.tvScore);
            ratingBarValuation = (RatingBar) itemView.findViewById(R.id.ratingBarValuation);

        }


    }
}
