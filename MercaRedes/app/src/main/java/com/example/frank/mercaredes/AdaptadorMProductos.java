package com.example.frank.mercaredes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by usuario on 14/06/2017.
 */

public class AdaptadorMProductos extends BaseAdapter{
    private ArrayList<Product> arrayListProd;
    private Context context;
    private LayoutInflater layoutInflater;


    public AdaptadorMProductos(Context context, ArrayList arrayListProd) {
        this.arrayListProd = arrayListProd;
        this.context = context;
    }

    @Override
    public int getCount() {
        return arrayListProd.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListProd.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vista = layoutInflater.inflate(R.layout.layout_mproductos, parent ,false);
        ImageView img = (ImageView) vista.findViewById(R.id.imageView7);
        TextView nombre = (TextView) vista.findViewById(R.id.textView15);
        TextView desc = (TextView) vista.findViewById(R.id.textView11);
        TextView prec = (TextView) vista.findViewById(R.id.textView6);

        Picasso.with(context).load(arrayListProd.get(position).getImage()).into(img);
        nombre.setText(arrayListProd.get(position).getTitulo());
        desc.setText(arrayListProd.get(position).getContenido());
        prec.setText(String.valueOf(arrayListProd.get(position).getPrecio()));


        return vista;
    }
}
