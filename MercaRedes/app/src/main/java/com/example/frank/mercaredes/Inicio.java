package com.example.frank.mercaredes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Inicio extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    boolean executeOnResume;
    FloatingActionButton mensaje;
    Button red;
    Button pedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /*Forzando la autenticacion*/
        executeOnResume = false;
        //Here we will check if the token is in shared preferences
        //If it is we will check it with the service if it is still valid
        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0); // 0 - for private mode
        //Testing purposes
        final SharedPreferences.Editor editor = pref.edit();
        //editor.clear();
        //editor.commit();
        //Recuperando datos
        String tokenRetrieved = pref.getString("authToken", null);

        if(TextUtils.isEmpty(tokenRetrieved)){
            Toast errTok = Toast.makeText(Inicio.this, "No token saved", Toast.LENGTH_SHORT);
            errTok.show();
            Intent intent = new Intent(Inicio.this, LoginActivity.class);
            startActivity(intent);


        } else {
            validateToken();
        }

        /*Fin de la autenticacion, los datos estan en shared preferences*/




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        red = (Button) findViewById(R.id.button8);
        pedido = (Button)findViewById(R.id.button9);

        pedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(),Main2Activity.class);
                startActivity(i);

            }
        });

        red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Inicio.this, MainActivity.class);
                startActivity(i);
            }
        });

        mensaje = (FloatingActionButton) findViewById(R.id.fab);
        mensaje.setEnabled(false);



    }

    protected void onResume(){
        super.onResume();
        if(executeOnResume){
            validateToken();
        } else {
            executeOnResume = true;
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_config) {
            Intent i = new Intent(this,Config.class);
            startActivity(i);
        } else if (id == R.id.nav_salir) {
            finish();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.nav_prod) {
            Intent i = new Intent(this, Productos.class);
            startActivity(i);
        }else if (id == R.id.nav_misprod){
            Intent i = new Intent(this,Mis_Productos.class);
            startActivity(i);
        }else if (id == R.id.nav_hacer_pedido) {
            Intent i = new Intent(this, Carrito.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




    protected void validateToken(){

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0); // 0 - for private mode
        //Testing purposes
        final SharedPreferences.Editor editor = pref.edit();
        String tokenRetrieved = pref.getString("authToken", null);
        Ion.with(Inicio.this)
                .load("GET", "http://pseesapiauth.herokuapp.com/check?token="+tokenRetrieved)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        if(e == null){
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(result);
                                String error = obj.optString("error");

                                if(TextUtils.isEmpty(error)){
                                    Integer id = obj.optInt("id");
                                    String email = obj.optString("email");
                                    String firstName = obj.optString("first_name");
                                    String lastName = obj.optString("last_name");
                                    String dui = obj.optString("dui");
                                    String municipality = obj.optString("municipality");
                                    JSONArray permjson = obj.getJSONArray("permissions");
                                    String[] permstring = permjson.toString().replace("},{", " ,").split(" ");
                                    Set<String> permissions = new HashSet<>(Arrays.asList(permstring));
                                    editor.putInt("id", id);
                                    editor.putString("email", email);
                                    editor.putStringSet("permissions", permissions);
                                    editor.putString("first_name", firstName);
                                    editor.putString("last_name", lastName);
                                    editor.putString("dui", dui);
                                    editor.putString("municipality", municipality);
                                    editor.commit();


                                } else {
                                    //Handle error
                                    Toast err = Toast.makeText(Inicio.this, "Expired", Toast.LENGTH_SHORT);
                                    err.show();
                                    editor.remove("authToken");
                                    editor.commit();
                                    Intent intent = new Intent(Inicio.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }


                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });


    }
}
