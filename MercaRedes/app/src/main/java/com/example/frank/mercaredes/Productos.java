package com.example.frank.mercaredes;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION_CODES.M;

public class Productos extends AppCompatActivity {

    private static String APP_DIRECTORY = "MyPictureApp/";
    private static String MEDIA_DIRECTORY = APP_DIRECTORY + "PictureApp";

    private final int MY_PERMISSIONS = 100;
    private final int PHOTO_CODE = 200;
    private final int SELECTED_PICTURE = 300;

    private ImageView img;
    private Button seleccionar;
    private RelativeLayout layout;
    private Button enviar;
    private EditText nombre;
    private EditText descripcion;
    Bitmap bit;
    private Spinner lista;
    private Spinner medidas;
    private Spinner catalogo;
    private EditText cantidad;
    private RadioButton disponible;
    private EditText precio;
    List<String> categorias = new ArrayList<String>();
    List<String> Idcat = new ArrayList<String>();
    List<String> uMedidas = new ArrayList<String>();
    List<String> IdMedidas = new ArrayList<String>();
    List<String> catalog = new ArrayList<String>();
    List<String> IdCatalogo = new ArrayList<String>();

    public static int idMedidas;
    public static int idCatalogos;
    public static int idCategorias;

    private String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);

        img = (ImageView)findViewById(R.id.imageView4);
        seleccionar = (Button)findViewById(R.id.button7);
        layout = (RelativeLayout)findViewById(R.id.layout1);
        enviar = (Button)findViewById(R.id.button3);
        nombre = (EditText)findViewById(R.id.editText2);
        descripcion = (EditText)findViewById(R.id.editText3);
        cantidad = (EditText)findViewById(R.id.editText8);
        cantidad.setText("Cantidad");
        disponible = (RadioButton)findViewById(R.id.radioButton);
        precio = (EditText)findViewById(R.id.editText7);
        precio.setText("Precio");
        new LlenarCategorias().execute();
        new LlenarMedidas().execute();
        new LlenarCatalogo().execute();
        lista = (Spinner)findViewById(R.id.spinner4);
        categorias.add("Seleccione Categoria");
        Idcat.add("0");

        medidas = (Spinner) findViewById(R.id.spinner2);
        uMedidas.add("Unidad de Medida");
        IdMedidas.add("0");

        catalogo = (Spinner)findViewById(R.id.spinner3);
        catalog.add("Seleccione el Catalogo");
        IdCatalogo.add("0");

        ArrayAdapter<String> adapt = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, uMedidas);
        medidas.setAdapter(adapt);
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categorias);
        lista.setAdapter(adaptador);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, catalog);
        catalogo.setAdapter(adapter);

        medidas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                idMedidas = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lista.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idCategorias = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        catalogo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idCatalogos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        if(myRequestStoragePermission()){
            seleccionar.setEnabled(true);
        }else{
            seleccionar.setEnabled(false);
        }

        seleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOption();
            }
        });

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(idCatalogos != 0 && idCategorias != 0 && idMedidas != 0 && nombre.getText().toString() != "Nombre del Producto"
                        && descripcion.getText().toString() != "Descripcion" && precio.getText().toString() != "Precio"
                        && cantidad.getText().toString() != "Cantidad" && bit != null && nombre.getText().toString() != "" && descripcion.getText().toString() != ""
                        && precio.getText().toString() != "" && cantidad.getText().toString() != "") {
                    new httpPost().execute();
                    nombre.setText("");
                    descripcion.setText("");
                    cantidad.setText("");
                    precio.setText("");
                    disponible.setChecked(false);
                } else {
                    Toast.makeText(Productos.this, "Ha dejado Campos sin llenar", Toast.LENGTH_SHORT).show();
                }
            }

        });

        nombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombre.setText("");
            }
        });

        descripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descripcion.setText("");
            }
        });

        cantidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cantidad.setText("");
            }
        });

        precio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                precio.setText("");
            }
        });


    }


    private boolean myRequestStoragePermission(){
        if(Build.VERSION.SDK_INT < M)
            return true;

        if((checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED))
            return true;

        if((shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) || (shouldShowRequestPermissionRationale(CAMERA))){
            Snackbar.make(layout, "Los permisos son necesarios para poder usar la aplicación",
                    Snackbar.LENGTH_INDEFINITE).setAction(android.R.string.ok, new View.OnClickListener() {

                @TargetApi(M)
                @Override
                public void onClick(View v) {
                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
                }
            });
        }else{
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, CAMERA}, MY_PERMISSIONS);
        }

        return false;
    }

    private void showOption(){
        final CharSequence[] option = {"Tomar foto", "Elegir de galeria", "Cancelar"};
        final AlertDialog.Builder builder = new AlertDialog.Builder(Productos.this);
        builder.setTitle("Elige una opción");
        builder.setItems(option, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(option[which] == "Tomar foto"){
                    openCamera();
                }else if(option[which] == "Elegir de galeria"){
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Selecciona Carpeta de imagenes"), SELECTED_PICTURE);
                }else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }

    private void openCamera() {
        File file = new File(Environment.getExternalStorageDirectory(), MEDIA_DIRECTORY);
        boolean isDirectoryCreated = file.exists();

        if(!isDirectoryCreated)
            isDirectoryCreated = file.mkdirs();

        if(isDirectoryCreated){
            Long timestamp = System.currentTimeMillis() / 1000;
            String imageName = timestamp.toString() + ".jpg";

            path = Environment.getExternalStorageDirectory() + File.separator + MEDIA_DIRECTORY
                    + File.separator + imageName;

            File newFile = new File(path);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(newFile));
            startActivityForResult(intent, PHOTO_CODE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("file_path", path);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        path = savedInstanceState.getString("file_path");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            switch (requestCode){
                case PHOTO_CODE:
                    MediaScannerConnection.scanFile(this,
                            new String[]{path}, null,
                            new MediaScannerConnection.OnScanCompletedListener() {
                                @Override
                                public void onScanCompleted(String path, Uri uri) {
                                    Log.i("ExternalStorage", "Scanned " + path + ":");
                                    Log.i("ExternalStorage", "-> Uri = " + uri);
                                }
                            });


                    Bitmap bitmap = BitmapFactory.decodeFile(path);
                    img.setImageBitmap(bitmap);
                    img.buildDrawingCache();
                    bit = img.getDrawingCache();
                    break;
                case SELECTED_PICTURE:
                    Uri path = data.getData();
                    img.setImageURI(path);
                    img.buildDrawingCache();
                    bit = img.getDrawingCache();
                    break;

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_PERMISSIONS){
            if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(Productos.this, "Permisos aceptados", Toast.LENGTH_SHORT).show();
                seleccionar.setEnabled(true);
            }
        }else{
            showExplanation();
        }
    }

    private void showExplanation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Productos.this);
        builder.setTitle("Permisos denegados");
        builder.setMessage("Para usar las funciones de la app necesitas aceptar los permisos");
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.show();
    }

    private class httpPost extends AsyncTask<String, Void, String>{

        String name = nombre.getText().toString();
        String desc = descripcion.getText().toString();
        String cant = cantidad.getText().toString();
        String disp = String.valueOf(disponible.isChecked());
        String prec = precio.getText().toString();

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0);
        Integer userId = pref.getInt("id", 0);

        @Override
        protected String doInBackground(String... params) {

            try{

                String idMed = IdMedidas.get(idMedidas);
                Log.e(idMed,"IdMedida");
                String idCat = Idcat.get(idCategorias);
                String idCatal=IdCatalogo.get(idCatalogos);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bit.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                String imageEncoded = Base64.encodeToString(b,Base64.DEFAULT);

                URL url = new URL("https://pseesapiproducts.herokuapp.com/products");

                JSONObject postDataParams = new JSONObject();


                postDataParams.put("product_name",name);
                postDataParams.put("participant_id",userId);
                postDataParams.put("product_catalog_id",idCatal);
                postDataParams.put("category_id",idCat);
                postDataParams.put("measure_id",idMed);
                postDataParams.put("description",desc);
                postDataParams.put("price",prec);
                postDataParams.put("stock",cant);
                postDataParams.put("available",disp);
                postDataParams.put("active","true");
                postDataParams.put("image",imageEncoded);

                Log.e("params",postDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

                }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();
        }

        }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

    public class LlenarCategorias extends AsyncTask<Void, Void, String>{

        String str="https://pseesapiproducts.herokuapp.com/categories";
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";

        @Override
        protected void onPostExecute(String result) {

            JSONObject json = null;
            try {

                JSONArray jsonarray = new JSONArray(result);//json.getJSONArray("contacts");
                for(int i=0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    String nombre = jsonobject.getString("category_name");
                    String id = jsonobject.getString("id");
                    categorias.add(nombre);
                    Idcat.add(id);
                    Log.e("App", nombre);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try
            {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background", ex);
            }
            return result;
        }
    }

    public class LlenarMedidas extends AsyncTask<Void, Void, String>{

        String str="https://pseesapiproducts.herokuapp.com/measures";
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";

        @Override
        protected void onPostExecute(String result) {

            JSONObject json = null;
            try {

                JSONArray jsonarray = new JSONArray(result);//json.getJSONArray("contacts");
                for(int i=0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    String nombre = jsonobject.getString("abbreviation");
                    String id = jsonobject.getString("id");
                    uMedidas.add(nombre);
                    IdMedidas.add(id);
                    Log.e("App", nombre);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try
            {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background", ex);
            }
            return result;
        }
    }

    public class LlenarCatalogo extends AsyncTask<Void, Void, String>{

        String str="https://pseesapiproducts.herokuapp.com/product_catalogs";
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";

        @Override
        protected void onPostExecute(String result) {

            JSONObject json = null;
            try {

                JSONArray jsonarray = new JSONArray(result);//json.getJSONArray("contacts");
                for(int i=0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    String nombre = jsonobject.getString("catalog_name");
                    String id = jsonobject.getString("id");
                    catalog.add(nombre);
                    IdCatalogo.add(id);
                    Log.e("App", nombre);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try
            {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background", ex);
            }
            return result;
        }
    }
}
