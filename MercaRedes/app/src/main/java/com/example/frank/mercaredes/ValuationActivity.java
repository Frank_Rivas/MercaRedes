package com.example.frank.mercaredes;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import Entidades.Valuation;

public class ValuationActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    ValuationAdapter valuationAdapter;
    RecyclerView listCommentary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valuation);
        progressDialog = new ProgressDialog(ValuationActivity.this);
        new TareaAsync().execute();
    }

    public class TareaAsync extends AsyncTask<Void, Void, String> {
        //Id del producto solicitado

        String ids = getIntent().getStringExtra("id");
        String productId = ids;
        //URL donde se encuentra la API Valuations.
        String valuationsUrl = "https://pseesapivaluations.herokuapp.com/values/"+productId+".json";
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";



        @Override
        protected String doInBackground(Void... params) {

            try
            {
                URL url = new URL(valuationsUrl);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {

                    Log.e("App", "En background 6");
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background e", ex);
            }
            return result;
        }

        @Override
        protected void onPreExecute() {

            //Se establecen los parametros del dialogo de proceso tipo,titulo,mensaje y si es posible cancelarlo.
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle("Procesando");
            progressDialog.setMessage("Por favor espere....");
            progressDialog.setCancelable(false);
            progressDialog.show();


        }

        @Override
        protected void onPostExecute(String result) {

            if(result.equals("[]")){
                Toast.makeText(getBaseContext(), "Este Producto no tiene Comentarios", Toast.LENGTH_SHORT).show();
                finish();
            }

            progressDialog.dismiss();

            List<Valuation> listValuation= new ArrayList<>();

            Toast.makeText(getBaseContext(), "Recibido!", Toast.LENGTH_LONG).show();

            String comentario ="";
            try {
                JSONArray jsonArray = new JSONArray(result);
                for(int i =0;i<jsonArray.length();i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    Valuation dataValuation = new Valuation();

                    if(jsonObject.getString("score")!="null")
                    {
                        dataValuation.firstName = jsonObject.getString("first_name");
                        dataValuation.lastName = jsonObject.getString("last_name");
                        dataValuation.score = Float.parseFloat(jsonObject.getString("score"));
                        if(jsonObject.getString("commentary")=="null")
                        {
                            dataValuation.commentary = "No ha escrito un comentario.";
                        }
                        else{
                            dataValuation.commentary = jsonObject.getString("commentary");
                        }


                        listValuation.add(dataValuation);
                    }

                }
                listCommentary= (RecyclerView) findViewById(R.id.listCommentary);
                valuationAdapter = new ValuationAdapter(ValuationActivity.this, listValuation);
                listCommentary.setAdapter(valuationAdapter);
                listCommentary.setLayoutManager(new LinearLayoutManager(ValuationActivity.this));


            } catch (JSONException e) {
                e.printStackTrace();

            }

        }


    }
}
