package com.example.frank.mercaredes;

/**
 * Created by Cristian on 17/06/2017.
 */

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Cristian on 16/06/2017.
 */

public class Lista_pedidos extends Fragment {
    ArrayList<Productos_Mensaje> ArrayProductos=null;
    ListView Listado=null;
    Adapter_Productos adapter = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.pedidos_lista, container, false);
        Listado=(ListView)rootView.findViewById(R.id.mispedidos);
        ArrayProductos=new ArrayList<>();
        new Buzon().execute();
        return rootView;
    }



    public class Buzon extends AsyncTask<Void, Void, String> {



        //final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0);
        final SharedPreferences prof = getActivity().getSharedPreferences("psees",0);
        Integer userId = prof.getInt("id", 0);

        String identificador= String.valueOf(userId);

        String str="https://pseesapipedidos.herokuapp.com/orders/supplier/"+identificador;
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";

        @Override
        protected void onPostExecute(String result) {

            JSONObject json = null;
            try {

                JSONArray jsonarray = new JSONArray(result);
                for(int i=0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);


                    int id_order=jsonobject.getInt("id");
                    int id_supplier=jsonobject.getInt("supplier_id");
                    int id_client=jsonobject.getInt("client_id");
                    String date=jsonobject.getString("date_delivery");
                    String status=jsonobject.getString("status");
                    int Fragment=2;

                    ArrayProductos.add(new Productos_Mensaje(id_order,id_supplier,id_client,date,status,Fragment));
                    Collections.sort(ArrayProductos, new Comparator<Productos_Mensaje>() {
                        @Override
                        public int compare(Productos_Mensaje p1, Productos_Mensaje p2) {
                            return new Integer(p2.getId_order()).compareTo(new Integer(p1.getId_order()));
                        }
                    });
                    addList();

                    Log.e("App", date);
                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try
            {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background", ex);
            }
            return result;
        }
    }

    private void addList(){

        adapter = new Adapter_Productos(getActivity(),ArrayProductos);
        Listado.setAdapter(adapter);
    }





}
