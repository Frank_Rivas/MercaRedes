package com.example.frank.mercaredes;

/**
 * Created by Cristian on 17/06/2017.
 */

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Fragment_recibidos extends Fragment {

    ArrayList<mensajes> ArrayMensajes=null;
    ListView Listado=null;
    AdapterMensaje adapter = null;
    int id_client;
    int id_supplier;
    int Fragment, id_order;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.mensajes_lista, container, false);
        Listado=(ListView)view.findViewById(R.id.Listado_mensajes);
        ArrayMensajes=new ArrayList<>();

        id_client = this.getArguments().getInt("parametro");
        id_supplier = this.getArguments().getInt("parametro2");
        Fragment=this.getArguments().getInt("parametro3");
        id_order=this.getArguments().getInt("parametro4");

        new Buzon().execute();

        Listado.setClickable(true);
        Listado.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        Listado.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);




       /* Listado.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getContext(),"mantuvo precionado",Toast.LENGTH_SHORT).show();
                return false;
            }
        });
      */

      /*  Listado.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                //Object o = listView.getItemAtPosition(position);
                // Realiza lo que deseas, al recibir clic en el elemento de tu listView determinado por su posicion.
                Log.i("Click", "click en el elemento " + position + " de mi ListView");
                Toast.makeText(getContext(),"click en el elemento " + position + " de mi ListView",Toast.LENGTH_SHORT).show();

            }
        });
        */

        ((Main3Activity) getActivity()).getSupportActionBar().setTitle("Bandeja de Entrada");

        setHasOptionsMenu (true);
        return view ;


    }
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.delete).setVisible(true);
    }



    public class Buzon extends AsyncTask<Void, Void, String> {


        String str="https://apimessage.herokuapp.com/order_message_";
        URLConnection urlConn = null;
        BufferedReader bufferedReader = null;
        String result = "";

        @Override
        protected void onPostExecute(String result) {

            JSONObject json = null;
            try {

                JSONArray jsonarray = new JSONArray(result);
                for(int i=0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    //String order_id = jsonobject.getString("order_id");
                    String message = jsonobject.getString("message");
                    //Boolean route = jsonobject.getBoolean("route");
                    String fecha = jsonobject.getString("created_at");
                    int id=jsonobject.getInt("id");
                    int order_id=jsonobject.getInt("order_id");

                    if (order_id==id_order) {
                        ArrayMensajes.add(new mensajes(message, fecha, id));

                        Collections.sort(ArrayMensajes, new Comparator<mensajes>() {
                            @Override
                            public int compare(mensajes p1, mensajes p2) {
                                return new Integer(p2.getId_mensaje()).compareTo(new Integer(p1.getId_mensaje()));
                            }
                        });

                        addList();

                        Log.e("App", message);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();

            }
        }

        @Override
        protected String doInBackground(Void... params) {
            if (Fragment==1)
            {
                str=str+"client"+"_received/"+String.valueOf(id_client);
            }
            else
            {
                str=str+"suppliers"+"_received/"+String.valueOf(id_supplier);
            }

            try
            {
                URL url = new URL(str);
                urlConn = url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    result += line;
                }

            }
            catch(Exception ex)
            {
                Log.e("App", "En background", ex);
            }
            return result;
        }
    }

    private void addList(){

        adapter = new AdapterMensaje(getContext(),ArrayMensajes);
        Listado.setAdapter(adapter);
    }

}
