package com.example.frank.mercaredes;

/**
 * Created by Cristian on 17/06/2017.
 */

public class mensajes implements  Comparable<mensajes> {

    private String texto;
    // private boolean route;
    //private String order_id;
    private String fecha;
    private  int id_mensaje;

    public int getId_mensaje() {
        return id_mensaje;
    }

    public void setId_mensaje(int id_mensaje) {
        this.id_mensaje = id_mensaje;
    }

    public mensajes(String texto, String fecha,int id) {
        this.texto = texto;
        this.fecha = fecha;
        this.id_mensaje=id;
    }

    @Override
    public int compareTo( mensajes o) {
        if (this.id_mensaje < o.id_mensaje) {
            return -1;
        }
        else
        if (this.id_mensaje > o.id_mensaje) {
            return 1;
        }
        else
            return 0;
    }

    @Override
    public String toString() {
        return "mensajes{" +
                "texto='" + texto + '\'' +
                ", fecha=" + fecha +
                '}';
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    // public boolean isRoute() {
    //   return route;
    //}

    /* public void setRoute(boolean route) {
         this.route = route;
     }

     public String getOrder_id() {
         return order_id;
     }

     public void setOrder_id(String order_id) {
         this.order_id = order_id;
     }
 */
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


}