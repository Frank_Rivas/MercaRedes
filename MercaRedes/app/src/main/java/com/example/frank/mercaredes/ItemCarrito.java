package com.example.frank.mercaredes;

/**
 * Created by usuario on 18/06/2017.
 */

public class ItemCarrito {
    private int id;
    private String image;
    private String titulo;
    private String contenido;
    private Double precio;
    private int cantidad;

    public ItemCarrito(int id,String image, String titulo, String contenido, double precio, int cantidad) {
        this.image = image;
        this.titulo = titulo;
        this.contenido = contenido;
        this.precio = precio;
        this.id = id;
        this.cantidad = cantidad;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    @Override
    public String toString() {
        return "Id: "+getId()+" Nombre: "+getTitulo()+" Precio: "+getPrecio()+" Cantidad: "+getCantidad();
    }
}
