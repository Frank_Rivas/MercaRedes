package com.example.frank.mercaredes;

import android.app.Application;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by benjamin on 6/18/17.
 */
public class MyApplication extends Application {
    private ArrayList<ItemCarrito> itemsCarrito = new ArrayList<ItemCarrito>();
    private static MyApplication singleInstance=null;

    public static MyApplication getInstance(){
        return singleInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleInstance = this;
    }

    public ArrayList<ItemCarrito> getItemsCarrito() {
        return itemsCarrito;
    }

    public void addItems(ItemCarrito item){
        this.itemsCarrito.add(item);
    }

    public void deleteItem(ItemCarrito item){
        this.itemsCarrito.remove(item.getId());
    }

    public void setItemsCarrito(ArrayList<ItemCarrito> itemsCarrito) {
        this.itemsCarrito = itemsCarrito;
    }
}
