package com.example.frank.mercaredes;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class Config extends AppCompatActivity {

    private TextView correo;
    private TextView nombre;
    private TextView apellido;
    private TextView municipalidad;
    private TextView dui;
    ImageButton setNombre;
    ImageButton setApellido;
    ImageButton setCorreo;
    ImageButton setMuni;
    ImageButton setDui;
    Button editar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        correo = (TextView)findViewById(R.id.editText6);
        nombre = (TextView)findViewById(R.id.texto_mensaje);
        apellido = (TextView)findViewById(R.id.editText5);
        municipalidad = (TextView) findViewById(R.id.editText10);
        dui = (TextView) findViewById(R.id.editText9);
        setNombre = (ImageButton)findViewById(R.id.imageButton);
        setApellido= (ImageButton)findViewById(R.id.imageButton3);
        setCorreo = (ImageButton)findViewById(R.id.imageButton5);
        setMuni = (ImageButton)findViewById(R.id.imageButton6);
        setDui = (ImageButton)findViewById(R.id.imageButton2);
        editar = (Button)findViewById(R.id.button6);
        editar.setEnabled(false);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0);
       // String userId = pref.getString("id", null);
        String userEmail = pref.getString("email", null);
        String firstName = pref.getString("first_name", null);
        String lastName = pref.getString("last_name", null);
        String muni = pref.getString("municipality", null);
        String Ndui = pref.getString("dui", null);
        nombre.setText(firstName);
        correo.setText(userEmail);
        apellido.setText(lastName);
        municipalidad.setText(muni);
        dui.setText(Ndui);

        nombre.setEnabled(false);
        correo.setEnabled(false);
        apellido.setEnabled(false);
        municipalidad.setEnabled(false);
        dui.setEnabled(false);

        setNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombre.setEnabled(true);
                editar.setEnabled(true);
            }
        });
        setApellido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apellido.setEnabled(true);
                editar.setEnabled(true);
            }
        });
        setCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                correo.setEnabled(true);
                editar.setEnabled(true);
            }
        });
        setMuni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                municipalidad.setEnabled(true);
                editar.setEnabled(true);
            }
        });
        setDui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dui.setEnabled(true);
                editar.setEnabled(true);
            }
        });


        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new editar().execute();
            }
        });
    }

    private class editar extends AsyncTask<String, Void, String>{

        String name = nombre.getText().toString();
        String apellid = apellido.getText().toString();
        String coreo = correo.getText().toString();
        String DUI = dui.getText().toString();
        String mun = municipalidad.getText().toString();

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("psees", 0);
        Integer userId = pref.getInt("id", 0);

        String urls = "https://pseesapiauth.herokuapp.com/credentials/" + userId;


        @Override
        protected String doInBackground(String... params) {
            try{

                URL url = new URL(urls);

                JSONObject putDataParams = new JSONObject();

                putDataParams.put("email",coreo);
                putDataParams.put("first_name",name);
                putDataParams.put("last_name",apellid);
                putDataParams.put("dui",DUI);
                putDataParams.put("municipality",mun);

                Log.e("params",putDataParams.toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setReadTimeout(15000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("PUT");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(putDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(new
                            InputStreamReader(
                            conn.getInputStream()));

                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    return sb.toString();

                }
                else {
                    return new String("false : "+responseCode);
                }
            }
            catch(Exception e){
                return new String("Exception: " + e.getMessage());
            }

        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), result,
                    Toast.LENGTH_LONG).show();
        }

    }


    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}
