package com.example.frank.mercaredes;

/**
 * Created by Cristian on 17/06/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Created by Cristian on 16/06/2017.
 */

public class Adapter_Productos extends BaseAdapter implements Serializable {

    private ArrayList<Productos_Mensaje>arrayList;
    private Context context;
    private LayoutInflater layoutInflater;


    public Adapter_Productos(Context context, ArrayList arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public int getCount( ) {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View visibles= layoutInflater.inflate(R.layout.pedidos_item, parent ,false);
        TextView fecha=(TextView)visibles.findViewById(R.id.fecha);
        TextView texto=(TextView)visibles.findViewById(R.id.status);
        TextView id_order=(TextView)visibles.findViewById(R.id.id_order);
        fecha.setText(arrayList.get(position).getDate());
        id_order.setText(arrayList.get(position).getStatus());
        texto.setText(String.valueOf(arrayList.get(position).getId_order()));

        FloatingActionButton fab=(FloatingActionButton)visibles.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, Main3Activity.class);
                i.putExtra("parametro",arrayList.get(position));
                context.startActivity(i);
                /*Toast.makeText(context,String.valueOf(arrayList.get(position).getId_order())+
                                " "+ String.valueOf(arrayList.get(position).getId_client())+ String.valueOf(arrayList.get(position).getId_supplier())
                        ,Toast.LENGTH_LONG).show();
                */


            }
        });

        return visibles;


    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public ArrayList<Productos_Mensaje> getCurrentCheckedPosition() {
        return this.arrayList;
    }





}